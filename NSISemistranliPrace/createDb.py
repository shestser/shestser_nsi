import sqlite3

conn = sqlite3.connect('wifi_data.db')

cursor = conn.cursor()

cursor.execute('''
    CREATE TABLE IF NOT EXISTS wifi_data (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        ssid TEXT,
        bssid TEXT,
        rssi INTEGER,
        location TEXT,
        timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
        
    )
''')

conn.commit()

conn.close()
