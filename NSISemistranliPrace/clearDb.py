import sqlite3

def clear_wifi_data_table():
    try:
        conn = sqlite3.connect('wifi_data.db')
        cursor = conn.cursor()
        cursor.execute('DELETE FROM wifi_data')
        conn.commit()
        print("wifi_data_realtime is clear.")

    except sqlite3.Error as e:
        print("Error", e)

    finally:
        if conn:
            conn.close()




clear_wifi_data_table()