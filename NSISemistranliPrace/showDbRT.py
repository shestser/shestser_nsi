import sqlite3
from prettytable import PrettyTable

conn = sqlite3.connect('wifi_data_realtime.db')
cursor = conn.cursor()

cursor.execute("SELECT * FROM wifi_data_realtime")

rows = cursor.fetchall()

table = PrettyTable()
table.field_names = ["ID", "SSID", "BSSID", "RSSI", "Location", "Timestamp"]

for row in rows:
    table.add_row(row)

print(table)

conn.close()
