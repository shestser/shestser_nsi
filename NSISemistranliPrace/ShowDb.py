import sqlite3
from prettytable import PrettyTable

conn = sqlite3.connect('wifi_data.db')
cursor = conn.cursor()

cursor.execute("SELECT COUNT(*) FROM wifi_data")
count = cursor.fetchone()[0]
print("Total records in wifi_data:", count)


cursor.execute("SELECT * FROM wifi_data")

rows = cursor.fetchall()

table = PrettyTable()
table.field_names = ["ID", "SSID", "BSSID", "RSSI", "Location", "Timestamp"]

for row in rows:
    table.add_row(row)

print(table)

conn.close()
