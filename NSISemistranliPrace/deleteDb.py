import os

def delete_database(db_path):
    """Delete the SQLite database file if it exists."""
    try:
        if os.path.exists(db_path):
            os.remove(db_path)
            print(f"Database '{db_path}' deleted successfully.")
        else:
            print(f"Database '{db_path}' does not exist.")
    except Exception as e:
        print(f"An error occurred while trying to delete the database: {e}")

if __name__ == "__main__":
    db_path = "wifi_data_realtime.db" 
    delete_database(db_path)
