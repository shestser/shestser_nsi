import sqlite3
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import accuracy_score

conn = sqlite3.connect('wifi_data.db')
query = "SELECT * FROM wifi_data"
data = pd.read_sql_query(query, conn)
conn.close()

le = LabelEncoder()
data['BSSID_encoded'] = le.fit_transform(data['bssid'])

X = data[['rssi', 'BSSID_encoded']]
y = data['location']


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Learning
model = RandomForestClassifier(n_estimators=1000, random_state=420)
model.fit(X_train, y_train)

# Accuracy
y_pred = model.predict(X_test)
accuracy = accuracy_score(y_test, y_pred)
print(f'Accuracy: {accuracy * 100:.2f}%')


conn = sqlite3.connect('wifi_data_realtime.db')
query = "SELECT * FROM wifi_data_realtime"  
realtime_data = pd.read_sql_query(query, conn)
conn.close()

try:
    realtime_data['BSSID_encoded'] = le.transform(realtime_data['bssid'])
except ValueError as e:
    unseen_labels = set(realtime_data['bssid']) - set(le.classes_)
    if unseen_labels:
    
        
        default_label = -1
        unseen_label_mapping = {label: default_label for label in unseen_labels}
        
        transformed_bssids = []
        for label in realtime_data['bssid']:
            if label in unseen_label_mapping:
                transformed_bssids.append(unseen_label_mapping[label])
            else:
                transformed_bssids.append(le.transform([label])[0])
                
        realtime_data['BSSID_encoded'] = transformed_bssids

mean_rssi = realtime_data['rssi'].mean()
mode_bssid = realtime_data['BSSID_encoded'].mode()[0] 
aggregated_features = pd.DataFrame({'rssi': [mean_rssi], 'BSSID_encoded': [mode_bssid]})

prediction = model.predict(aggregated_features)
print(f"Predicted location: {prediction[0]}")


print(realtime_data.head())
