import serial
import sqlite3

ser = serial.Serial('COM6', 9600)

conn = sqlite3.connect('wifi_data.db')
cursor = conn.cursor()

def parse_and_insert_data(data):
    parts = data.split(", ")
    ssid = parts[0].split(": ")[1]
    bssid = parts[1].split(": ")[1]
    rssi = int(parts[2].split(": ")[1])
    location = parts[3].split(": ")[1] if len(parts) > 3 else "unknown"

    
    cursor.execute('''
        INSERT INTO wifi_data (ssid, bssid, rssi, location) 
        VALUES (?, ?, ?, ?)
    ''', (ssid, bssid, rssi,location))
    print("data saved")
    conn.commit()


while True:
    line = ser.readline().decode('utf-8').strip()
    if line.startswith('SSID:'):
        parse_and_insert_data(line)

ser.close()
conn.close()
